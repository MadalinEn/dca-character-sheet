var CharacterJSON = {
	"Name": "Maverick",
	"Identity": "Jack Jacobs",
	"PowerLevel": 9,
	"Abilities": {
		"Strength": {
			"baseValue": 3,
			"modifier": 0
		},
		"Stamina": {
			"baseValue": 3
		},
		"Agility": {
			"baseValue": 0
		},
		"Dexterity": {
			"baseValue": 0
		},
		"Fighting": {
			"baseValue": 5,
			"modifier": 0
		},
		"Intellect": {
			"baseValue": 4
		},
		"Awareness": {
			"baseValue": 1
		},
		"Presence": {
			"baseValue": 0
		}
	},
	"Defenses": {
		"Dodge": {
			"modifier": 1
		},
		"Parry": {
			"modifier": 1
		},
		"Fortitude": {
			"modifier": 1
		},
		"Willpower": {
			"modifier": 3
		},
		"Toughness": {
			"modifier": 0
		},
		"Initiative": {}
	},
// ============================= SKILLS ============================= //
	"Skills": {
		"Acrobatics" : {
			"ability": "Agility",
			"trained": false
		},
		"Athletics" : {
			"ability": "Strength"
		},
		"Close Combat" : {
			"ability": "Fighting"
		},
		"Close Combat [Unarmed]" : {
			"ability": "Fighting",
			"modifier": 2
		},
		"Deception" : {
			"ability": "Presence",
			"modifier": 0
		},
		"Expertise [Science]" : {
			"ability": "Intellect",
			"modifier": 2
		},
		"Expertise [Engineering]" : {
			"ability": "Intellect",
			"modifier": 2
		},
		"Insight" : {
			"ability": "Awareness"
		},
		"Intimidation" : {
			"ability": "Presence"
		},
		"Investigation" : {
			"ability": "Intellect"
		},
		"Perception" : {
			"ability": "Awareness"
		},
		"Persuasion" : {
			"ability": "Presence"
		},
		"Rngd Combat" : {
			"ability": "Dexterity"
		},
		"Rngd Combat [Energy Beams]" : {
			"ability": "Dexterity",
			"modifier": 8
		},
		"Sleight of Hand" : {
			"ability": "Dexterity",
			"trained": false
		},
		"Stealth" : {
			"ability": "Agility",
			"modifier": 3
		},
		"Technology" : {
			"ability": "Intellect",
			"tools": true,
			"modifier": 11
		},
		"Treatment" : {
			"ability": "Intellect",
			"tools": true,
			"modifier": 2
		},
		"Vehicles" : {
			"ability": "Dexterity",
			"trained": false
		}
	},
// ============================= POWERS ============================= //
	"Powers": {
		"Primary" : {
			"icon": "person_outline",
			"type": "normal",
			"PowerComponents" : {
				"Machine Language" : {
					"rank": 2,
					"effect": {
						"description" : "Comprehend: Machies/Electronics"
					}
				},
				"Removable Nano-Suit" : {
					"rank": 14,
					"effect": {
						"description" : "Suit is removable under certain conditions"
					}
				},
				"Shapeshifting Suit" : {
					"rank": 2,
					"effect": {
						"description" : "Feature: Shapeshift into any outfit"
					}
				},
				"Comm System" : {
					"rank": 1,
					"effect": {
						"description" : "Communication: Radio"
					}
				},
				"Nanocomposite Armor" : {
					"rank": 5,
					"effect": {
						"description" : "Protection",
						"enhancements": [{
							"statGroup": "Defenses",
							"stat": "Toughness"
						}]
					}
				},
				"Thrusters" : {
					"rank": 2,
					"effect": {
						"description" : "Flight",
						"interaction": {
							"modifier": 2
						},
					}
				}
			}
		},
		"Tactical Computer" : {
			"icon": "blur_linear",
			"type": "normal",
			"PowerComponents" : {
				"Combat Sense" : {
					"rank": 1,
					"effect": {
						"description" : "Enhanced: Fighting",
						"enhancements": [{
							"statGroup": "Abilities",
							"stat": "Fighting"
						}]
					}
				},
				"Combat Reflexes" : {
					"rank": 1,
					"effect": {
						"description" : "Enhanced: Dodge",
						"enhancements": [{
							"statGroup": "Defenses",
							"stat": "Dodge"
						}]
					}
				},
				"Combat Resilience" : {
					"rank": 2,
					"effect": {
						"description" : "Enhanced: Fortitude",
						"enhancements": [{
							"statGroup": "Defenses",
							"stat": "Fortitude"
						}]
					}
				}
			}
		},
		"Utility": {
			"icon": "polymer",
			"type": "dynamic",
			"powerPoints": 12,
			"PowerComponents" : {
				"Maximum Mobility": {
					"type": "dynamic",
					"pointsPerRank": 2,
					"minVal": 0,
					"effect": {
						"description" : "Enhanced: Agility",
						"enhancements": [{
							"statGroup": "Abilities",
							"stat": "Agility"
						}]
					}
				},
				"Maximum Power": {
					"type": "dynamic",
					"pointsPerRank": 2,
					"minVal": 0,
					"effect": {
						"description" : "Enhanced: Strength",
						"enhancements": [{
							"statGroup": "Abilities",
							"stat": "Strength"
						}]
					}
				},
				"Mind Shield": {
					"type": "dynamic",
					"pointsPerRank": 1,
					"minVal": 0,
					"maxVal": 12,
					"effect": {
						"description" : "Enhanced: Will",
						"enhancements": [{
							"statGroup": "Defenses",
							"stat": "Willpower",
							"startsAfter": 2
						}]
					}
				},
				"Sensors": {
					"type": "dynamic",
					"pointsPerRank": 1,
					"minVal": 0,
					"effect": {
						"description" : "Senses: Darkvision, Infravision, Extended (1) {x100ft} Accurate Radio, Counters Tech Concealment Extended (1) {x100ft} Accurate Acute Ultra-Hearing",
						"interaction": {
							"statGroup": "Skills",
							"stat": "Perception"
						}
					}
				},
				"Electric Vision": {
					"type": "alternate",
					"pointsPerRank": 1,
					"minVal": 0,
					"effect": {
						"description" : "Senses: Detect Energy (Electicity), Ranged, Accurate, Acute, Analytical, Extended (2), Penetrates Concealment",
						"interaction": {
							"statGroup": "Skills",
							"stat": "Perception"
						}
					}
				},
				"Life Support System": {
					"type": "dynamic",
					"pointsPerRank": 1,
					"minVal": 0,
					"maxVal": 11,
					"effect": {
						"description" : "Immunity: Immunity to disease, poison, all environmental conditions, suffocation, starvation and thirst; Immunity to need to sleep"
					}
				},
				"Molecular Reconstruction Matrix": {
					"type": "alternate",
					"pointsPerRank": 1,
					"minVal": 0,
					"maxVal": 10,
					"effect": {
						"description" : "Regeneration"
					}
				}
			}
		},
		"Ordnance": {
			"icon": "whatshot",
			"type": "array",
			"points": 16,
			"PowerComponents" : {
				"Repulsor Beams": {
					"type": "alternate",
					"effect": {
						"description" : "Blast: Kinetic Energy | Area (Burst|Cover) (4)",
						"interaction": {
							"type": "attack",
							"statGroup": "Skills",
							"stat": "Rngd Combat [Energy Beams]"
						},
						"damage": {
							"modifier": 6
						},
						"secondary-damage": {
							"modifier": 4
						}
					}
				},
				"Power Strike": {
					"type": "alternate",
					"effect": {
						"description" : "Affliction: {Fortitude} (Vulnerable > Defenseless > Incapacitated) | Improved Critical (3), Linked to Strength-Based Damage (5)",
						"interaction": {
							"type": "attack",
							"statGroup": "Skills",
							"stat": "Close Combat [Unarmed]",
							"improvedCritical": 3
						},
						"affliction": {
							"modifier": 8
						},
						"damage": {
							"statGroup": "Abilities",
							"stat": "Strength",
							"modifier": 5
						}
					}
				},
				"Enfeebling Touch": {
					"type": "alternate",
					"effect": {
						"description" : "Weaken: Close Range {Fortitude} | Progressive, Limited to 5 Charges",
						"interaction": {
							"charges": {
								"count": 5
							},
							"type": "attack",
							"statGroup": "Skills",
							"stat": "Close Combat [Unarmed]",
						},
						"weaken": {
							"modifier": 8
						},
						"damage": {
							"statGroup": "Abilities",
							"stat": "Strength"
						}
					}
				},
				"Mecha Wings": {
					"type": "alternate",
					"effect": {
						"description" : "Flight: Wings | Subtle (2)",
						"interaction": {
							"power": "Primary",
							"powerComponent": "Thrusters",
							"modifier": 14
						}
					}
				},
				"Hyper Thrusters": {
					"type": "alternate",
					"effect": {
						"description" : "Flight:  | Linked Movement (2) [Environmental Adaptation (Zero-G)], Space Travel (1)",
						"interaction": {
							"power": "Primary",
							"powerComponent": "Thrusters",
							"modifier": 6
						}
					}
				},
				"Lay On Hands": {
					"type": "alternate",
					"effect": {
						"description" : "Healing: Close Range | Affects Others Only",
						"interaction": {
							"modifier": 16
						}
					}
				}
			}
		},
		"Devices" : {
			"icon": "memory",
			"type": "normal",
			"PowerComponents" : {
				"Tactical Cloak" : {
					"type": "alternate",
					"rank": 5,
					"effect": {
						"description" : "Concealment: Visual (4), Auditory (1) | Easily Removable",
						"interaction": {
							"statGroup": "Skills",
							"stat": "Stealth",
						}
					}
				}
			}
		},
		"Pandora AI": {
			"icon": "blur_circular",
			"type": "normal",
			"PowerComponents" : {	
				"Enhanced Skills": {
					"rank": 0,
					"type": "normal",
					"effect": {
						"description" : "Enhanced Skills",
						"enhancements": [
							{
								"statGroup": "Skills",
								"stat": "Insight",
								"modifier": 13
							},
							{
								"statGroup": "Skills",
								"stat": "Investigation",
								"modifier": 12
							},
							{
								"statGroup": "Skills",
								"stat": "Perception",
								"modifier": 15
							}
						]
					}
				}
			}
		},
		"Power Stunts" : {
			"icon": "flash_on",
			"type": "dynamic",
			"powerPoints": 12,
			"PowerComponents" : {
				"Targeting Array" : {
					"type": "dynamic",
					"pointsPerRank": 1,
					"minVal": 0,
					"maxVal": 12,
					"effect": {
						"description" : "Enhanced Advantage: Ranged Combat",
						"enhancements": [
							{
								"statGroup": "Skills",
								"stat": "Rngd Combat"
							},
							{
								"statGroup": "Skills",
								"stat": "Rngd Combat [Energy Beams]"
							}
						]
					}
				},
				"Force Field: Protect" : {
					"rank": 16,
					"type": "alternate",
					"effect": {
						"description" : "Deflect: Ranged | Linked Regeneration (5)",
						"interaction": {
							"type": "luck",
							"modifier": 11
						}
					}
				},
				"Channel EMP" : {
					"rank": 8,
					"type": "alternate",
					"effect": {
						"description" : "Nullify: Technology | Broad, Concentration, Simultaneous; Reduced Range [Close], Side Effect: Backlash (disables Arsenal u.e.o.m.n.t)",
						"interaction": {
							// "charges": {
							// 	"count": 5
							// },
							"type": "attack",
							"statGroup": "Skills",
							"stat": "Close Combat [Unarmed]",
						},
						"nullify": {
							"modifier": 8
						},
						// "damage": {
						// 	"statGroup": "Abilities",
						// 	"stat": "Strength"
						// }
					}
				},
				"Disabling Strike": {
					"rank": 8,
					"type": "alternate",
					"effect": {
						"description" : "Affliction: {Fortitude} (Impaired > Disabled > Incapacitated) | Progressive, Linked to Strength-Based Damage; Limited to 5 charges",
						"interaction": {
							"charges": {
								"count": 5
							},
							"type": "attack",
							"statGroup": "Skills",
							"stat": "Close Combat [Unarmed]"
						},
						"affliction": {
							"modifier": 8
						},
						"damage": {
							"statGroup": "Abilities",
							"stat": "Strength"
						}
					}
				},
				"Homing Missile": {
					"rank": 8,
					"type": "alternate",
					"effect": {
						"description" : "Blast | Limited to 5 charges; Accurate (5), Homing (2) Linked to Senses (1) [Infravision]",
						"interaction": {
							"charges": {
								"count": 5
							},
							"type": "attack",
							"statGroup": "Skills",
							"stat": "Rngd Combat",
							"modifier": 5
						},
						"damage": {
							"modifier": 8
						}
					}
				}
			}
		}

	},
// ============================= ADVANTAGES ============================= //
	"Advantages": {
		"Inventor": {
			"description": "You can use the Technology skill to create inventions."
		},
		"Improvised Tools": {
			"description": "You ignore the circumstance penalty for using skills without proper tools, since you can improvise sufficient tools with whatever is at hand. If you’re forced to work without tools at all, you suffer only a –2 penalty."
		},
		"Second Chance [Technology Checks]": {
			"description": "If you fail a check against that hazard, you can make another immediately and use the better of the two results. You only get one second chance for any given check."
		},
		"Luck": {
			"rank": 1,
			"description": "Once per round, you can choose to re-roll a die roll, like spending a hero point (see Hero Points, page 20), including adding 10 to re-rolls of 10 or less. You can do this a number of times per game session equal to your Luck rank, with a maximum rank of half the series power level (rounded down). Your Luck ranks refresh when your hero points “reset” at the start of an adventure. The GM may choose to set a different limit on ranks in this advantage, depending on the series.",
			"effect": {
				"power": {
					"icon": "",
					"type": "normal",
					"PowerComponents" : {
						"Re-Roll" : {
							"effect": {
								"description" : "Once per round, you can choose to re-roll a die roll, like spending a hero point (see Hero Points, page 20), including adding 10 to re-rolls of 10 or less. You can do this a number of times per game session equal to your Luck rank, with a maximum rank of half the series power level (rounded down). Your Luck ranks refresh when your hero points “reset” at the start of an adventure. The GM may choose to set a different limit on ranks in this advantage, depending on the series.",
								"interaction": {
									"charges": {
										"count": 1
									},
									"type": "luck",
									"modifier": 0
								}
							}
						}
					}
				}
				
			}
		},
		"Sidekick [Pandora AI]": {
			"rank": 5,
			"description": "",
			"effect": {
				"power": {
					"icon": "",
					"type": "normal",
					"PowerComponents" : {
						"Eidetic Memory" : {
							"type": "normal",
							"effect": {
								"description" : "You have perfect recall of everything you’ve experienced. You have a +5 circumstance bonus on checks to remember things, including resistance checks against effects that alter or erase memories. You can also make Expertise skill checks to answer questions and provide information as if you were trained, meaning you can answer questions involving difficult or obscure knowledge even without ranks in the skill, due to the sheer amount of trivia you have picked up.",
								"interaction": {
									"statGroup": "Abilities",
									"stat": "Intellect",
									"modifier": 5
								}
							}
						},
						"Well-Informed" : {
							"type": "normal",
							"effect": {
								"description" : "You are exceptionally well-informed. When encountering an individual, group, or organization for the first time, you can make an immediate Investigation or Persuasion skill check to see if your character has heard something about the subject. Use the guidelines for gathering information in the Investigation skill description to determine the level of information you gain. You receive only one check per subject upon first encountering them, although the GM may allow another upon encountering the subject again once significant time has passed.",
								"interaction": {
									"statGroup": "Skills",
									"stat": "Investigation"
								}
							}
						},
						"Universal Translator" : {
							"rank": 2,
							"type": "normal",
							"effect": {
								"description" : "Comprehend: Speak and understand any language | Activation (1) [Move Action], Distracting",
								"interaction": {
									"statGroup": "Abilities",
									"stat": "Intellect"
								}
							}
						},
						"Situational Awareness" : {
							"rank": 2,
							"type": "normal",
							"effect": {
								"description" : "Senses: Danger Sense, Rapid Vision (1)",
								"interaction": {
									"statGroup": "Skills",
									"stat": "Perception"
								}
							}
						},
						"Superior Insight" : {
							"rank": 2,
							"type": "normal",
							"effect": {
								"description" : "Enhanced Advantages: Assessment, Favored Foe (previously assesed)",
								"interaction": {
									"statGroup": "Skills",
									"stat": "Insight"
								},
								"enhancements": [
									{
										"statGroup": "Skills",
										"stat": "Deception"
									},
									{
										"statGroup": "Skills",
										"stat": "Intimidation"
									},
									{
										"statGroup": "Skills",
										"stat": "Insight"
									},
									{
										"statGroup": "Skills",
										"stat": "Perception"
									}
								]
							}
						},
						"Predictive Modeling" : {
							"rank": 1,
							"pointsPerRank": 3,
							"type": "normal",
							"effect": {
								"description" : "Luck Control: Bestow",
								"interaction": {
									"charges": {
										"count": 3
									},
									"type": "luck",
									"modifier": 0
								}
							}
						},
						"Causality Analysis": {
							"rank": 4,
							"type": "normal",
							"effect": {
								"description" : "Postcognition: | Activation (1) [Move Action], Distracting",
								"interaction": {
									"statGroup": "Skills",
									"stat": "Investigation"
								}
							}
						}
					}
				}
			}
		},
		"Equipment": {
			"rank": 2,
			"description": ""
		},
		"Favored Environment [Urban]": {
			"rank": 3,
			"description": "You have an environment you’re especially suited for fighting in. Examples include in the air, underwater, in space, in extreme heat or cold, in jungles or woodlands, and so forth. While you are in your favored environment, you gain a +2 circumstance bonus to attack checks or your active defenses. Choose at the start of the round whether the bonus applies to attack or defense. The choice remains until the start of your next round. This circumstance bonus is not affected by power level.",
			"effect": {
				"power" : {
					"icon": "",
					"type": "array",
					"PowerComponents" : {
						"Active Defenses" : {
							"type": "alternate",
							"rank": 2,
							"effect": {
								"description" : "Enhanced: Dodge and Parry",
								"enhancements": [
									{
										"statGroup": "Defenses",
										"stat": "Dodge"
									},
									{
										"statGroup": "Defenses",
										"stat": "Parry"
									}
								]
							}
						},
						"Attacks" : {
							"type": "alternate",
							"rank": 2,
							"effect": {
								"description" : "Enhanced: Attacks",
								"enhancements": [
									{
										"statGroup": "Skills",
										"stat": "Close Combat"
									},
									{
										"statGroup": "Skills",
										"stat": "Close Combat [Unarmed]"
									},
									{
										"statGroup": "Skills",
										"stat": "Rngd Combat"
									},
									{
										"statGroup": "Skills",
										"stat": "Rngd Combat [Energy Beams]"
									}
								]
							}
						}
					}
				}
			}
		},
		"Improved Critical [Unarmed]": {
			"rank": 1,
			"description": "Score critical hits on (20-rank) and above",
			"effect": {
				"improvedCritical": {
					"statGroup": "Skills",
					"stat": "Close Combat [Unarmed]"
				}
			}
		}
	},
	"Bio" : "<p>Origin of Nano-tattoo is unknown; gained at age 27. First power-suit created at age 28. Maverick MK 0 created at age 29</p>",
	"Complications": "<p>Responsibility | Secret identity | Job @ MarsTech | Relationship</p><p>Fear of not fitting in or not being good enough</p>",
	"theme": {
		"colorScheme": {
			"mainColor": "teal",
			"mainColorText": "teal-text",
			"accentColor": "red",
			"accentColorText": "red-text"
			// "mainColor": "darker-cyan",
			// "mainColorText": "cyan-text text-darken-1",
			// "accentColor": "amber darken-4",
			// "accentColorText": "amber-text text-darken-4"
		},
		"images": {
			"profile": "img/profile/maverick_profile.jpg",
			"banner": "img/profile/storm-breakers-banner.png",
			// "parallax": "img/bg/screen.png"
			"parallax": "https://s1.thcdn.com/widgets/96-en/11/580x312-z-wk30-ch-DCComics-022411.jpg",
		}
	}
};