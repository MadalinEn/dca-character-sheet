
function Ability(args) {
	var self = this;
	this.name = args.name;
	this.elemId = convertToId(self.name);
	this.baseValue = parseInt(args.baseValue || 0);
	this.modifier = parseInt(args.modifier || 0);
	this.tempMod = 0;
	this.rank = function() {
		return parseInt(self.baseValue) + parseInt(self.modifier) + parseInt(self.tempMod);
	};
}

Ability.prototype.getHTML = function() {
	var self = this;

	var html = '	\
	<div class="row">	\
	    <div class="col s9 abilityLabel stat-label">	\
	        <a class="modal-trigger interaction-trigger" href="#diceModal" stat-group="Abilities" stat="'+self.name+'"><span class="">'+self.name+'</span></a>	\
	    </div>	\
	    <div class="col s3 abilityVal stat-val">	\
	        <span id="'+self.elemId+'" class="">'+self.rank()+'</span>	\
	    </div>	\
	</div>';

	return html;
}
