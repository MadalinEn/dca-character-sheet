
function Advantage(args) {
	var self = this;
	this.name = args.name;
	this.elemId = convertToId(self.name);
	this.colorScheme = args.colorScheme;
	this.rank = parseInt(args.rank || 1);
	this.description = args.description || "";
	this.effect = args.effect || {};
}

Advantage.prototype.getHTML = function() {
	var self = this;
	var rankText = "";
	var secondaryContent = "";
	var itemContainer = $("<li>", {
		"class": "collection-item"
	});

	if(self.rank > 1) {
		rankText = "<["+self.rank+"]>";
	}


	if(typeof(self.effect.power) !== 'undefined') {
			itemContainer.addClass('advantage-power');
			
			var powCollapsible = $("<ul>", {
				"class": "collapsible",
				"data-collapsible": "expandable"
			});

	    	var pow = self.effect.power;

	    	var advantagePower = new Power({
				name: self.name,
				icon: pow.icon,
				colorScheme: self.colorScheme,
				pseudo: 'Advantages',
				type: pow.type || "normal",
				powerPoints: pow.powerPoints || 1,
				PowerComponents: pow.PowerComponents || {}
			});

			powCollapsible.append(advantagePower.getHTML());

	    	itemContainer.append(powCollapsible);
	}
	else {
		itemContainer.append(
			$("<div/>")
			.addClass("advantageLabel")
			.html(self.name +" "+ rankText)
			.append(secondaryContent)
		);
	}

	return itemContainer;
}
