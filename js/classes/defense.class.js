
function Defense(args) {
	var self = this;
	this.name = args.name;
	this.elemId = convertToId(self.name);
	this.ability = args.ability;
	this.modifier = parseInt(args.modifier || 0);
	this.tempMod = 0;
	this.rank = function() {
		return parseInt(self.ability.rank()) + parseInt(self.modifier) + parseInt(self.tempMod);
	};
}

Defense.prototype.getHTML = function() {
	var self = this;

	var html = '	\
	<div class="row">	\
	    <div class="col s9 defenseLabel stat-label">	\
	        <a class="modal-trigger interaction-trigger" href="#diceModal" stat-group="Defenses" stat="'+self.name+'"><span class="">'+self.name+'</span></a>	\
	    </div>	\
	    <div class="col s3 defenseVal stat-val">	\
	        <span id="'+self.elemId+'" class="">'+self.rank()+'</span>	\
	    </div>	\
	</div>';

	return html;
}

Defense.prototype.updateDefense = function(args) {
	var self = this;

}