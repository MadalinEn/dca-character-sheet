
function Skill(args) {
	var self = this;
	this.name = args.name;
	this.elemId = convertToId(self.name);
	this.trained = args.trained;
	this.tools = args.tools || false;
	this.ability = args.ability;
	this.modifier = parseInt(args.modifier || 0);
	this.tempMod = 0;
	this.rank = function() {
		return parseInt(self.ability.rank()) + parseInt(self.modifier) + parseInt(self.tempMod);
	};
	this.improvedCritical = 0;
}

Skill.prototype.getHTML = function() {
	var self = this;

	var html = '	\
	<div class="row '+ ((self.trained)? 'trained' : 'untrained') +'">	\
	    <div class="col s10 skillLabel stat-label">	\
	        <a class="modal-trigger interaction-trigger" href="#diceModal" stat-group="Skills" stat="'+self.name+'">	\
	        	<span class="">'+self.name+'</span>	\
	        	'+ ((self.tools)? '&nbsp<i class="tiny material-icons">build</i>' : '') +'	\
	        </a>	\
	    </div>	\
	    <div class="col s2 skillVal stat-val">	\
	        <span id="'+self.elemId+'" class="">'+self.rank()+'</span>	\
	    </div>	\
	</div>';

	return html;
}