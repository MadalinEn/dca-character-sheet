
function Character(args) {

	var self = this;

	this.Name = args.Name || "Nomen";
	this.Identity = args.Identity || "Nescio";
	this.PowerLevel = parseInt(args.PowerLevel || 10);
    this.Bio = args.Bio || "";
    this.Complications = args.Complications || "";
    this.theme = args.theme;

    this.Abilities = {
		"Strength": new Ability({
			name: "Strength",
			baseValue: args.Abilities['Strength'].baseValue || 0,
    		modifier: args.Abilities['Strength'].modifier || 0,
		}),
		"Stamina": new Ability({
			name: "Stamina",
			baseValue: args.Abilities['Stamina'].baseValue || 0,
    		modifier: args.Abilities['Stamina'].modifier || 0,
		}),
		"Agility": new Ability({
			name: "Agility",
			baseValue: args.Abilities['Agility'].baseValue || 0,
    		modifier: args.Abilities['Agility'].modifier || 0,
		}),
		"Dexterity": new Ability({
			name: "Dexterity",
			baseValue: args.Abilities['Dexterity'].baseValue || 0,
    		modifier: args.Abilities['Dexterity'].modifier || 0,
		}),
		"Fighting": new Ability({
			name: "Fighting",
			baseValue: args.Abilities['Fighting'].baseValue || 0,
    		modifier: args.Abilities['Fighting'].modifier || 0,
		}),
		"Intellect": new Ability({
			name: "Intellect",
			baseValue: args.Abilities['Intellect'].baseValue || 0,
    		modifier: args.Abilities['Intellect'].modifier || 0,
		}),
		"Awareness": new Ability({
			name: "Awareness",
			baseValue: args.Abilities['Awareness'].baseValue || 0,
    		modifier: args.Abilities['Awareness'].modifier || 0,
		}),
		"Presence": new Ability({
			name: "Presence",
			baseValue: args.Abilities['Presence'].baseValue || 0,
    		modifier: args.Abilities['Presence'].modifier || 0,
		})
    };

    this.Defenses = {
		"Dodge": new Defense({
			name: "Dodge",
			ability: self.Abilities['Agility'],
    		modifier: args.Defenses['Dodge'].modifier || 0,
		}),
		"Parry": new Defense({
			name: "Parry",
			ability: self.Abilities['Fighting'],
    		modifier: args.Defenses['Parry'].modifier || 0,
		}),
		"Fortitude": new Defense({
			name: "Fortitude",
			ability: self.Abilities['Stamina'],
    		modifier: args.Defenses['Fortitude'].modifier || 0,
		}),
		"Willpower": new Defense({
			name: "Willpower",
			ability: self.Abilities['Awareness'],
    		modifier: args.Defenses['Willpower'].modifier || 0,
		}),
		"Toughness": new Defense({
			name: "Toughness",
			ability: self.Abilities['Stamina'],
    		modifier: args.Defenses['Toughness'].modifier || 0,
		}),
		"Initiative": new Defense({
			name: "Initiative",
			ability: self.Abilities['Agility'],
    		modifier: args.Defenses['Initiative'].modifier || 0,
		})
	};

	this.Skills = {};
	for(var index in args.Skills) {
		var skill = args.Skills[index];
		var trained = true;

		if(typeof(skill.trained) !== 'undefined') {
			trained = skill.trained;
		}
		
		self.Skills[index] = new Skill({
			name: index,
			trained: trained,
			tools: skill.tools || false,
			ability: self.Abilities[skill.ability],
			modifier: skill.modifier || 0
		});
	}
	this.Powers = {};
	for(var index in args.Powers) {
		var power = args.Powers[index];
		self.Powers[index] = new Power({
			name: index,
			icon: power.icon || "flash_on",
			colorScheme: self.theme.colorScheme,
			type: power.type || "normal",
			powerPoints: power.powerPoints || 1,
			PowerComponents: power.PowerComponents || {}
		});
	}

	this.Advantages = {};
	for(var index in args.Advantages) {
		var advantage = args.Advantages[index];
		self.Advantages[index] = new Advantage({
			name: index,
			colorScheme: self.theme.colorScheme,
			rank: advantage.rank,
			description: advantage.description,
			effect: advantage.effect
		});

		if('effect' in advantage) {
			if('improvedCritical' in advantage.effect) {
				var statGroup = advantage.effect.improvedCritical.statGroup;
				var stat = advantage.effect.improvedCritical.stat;
				self[statGroup][stat].improvedCritical = advantage.rank;
			}
		}
	}

	/*this.Attacks = {};
	for(var index in args.Attacks) {
		var attack = args.Attacks[index];
		self.Attacks[index] = new Attack({
			name: index,
			rank: attack.rank,
			description: attack.description,
			effect: attack.effect
		});
	}*/

}

/**
 * initialize the character
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
Character.prototype.init = function(args) {
	var self = this;
	self.getAbilitiesSectionHTML();
	self.getDefensesSectionHTML();
	self.getSkillsSectionHTML();
	self.getPowersSectionCollapsibleHTML();
	self.getAdvantagesSectionHTML();
	self.getBioSectionHTML();
	self.setTheme();



	/*=============================================
	=            Dynamic event binders            =
	=============================================*/

	$(document).on('click', '.stat-val', function(event) {
		$("#tempModal").children('.modal-content').html('');
		$("#resetTempMod").unbind( "click" );
		$('#tempModal').modal('open');

		var stat = $(this).siblings('.stat-label').find('.interaction-trigger').attr('stat');
		var statGroup = $(this).siblings('.stat-label').find('.interaction-trigger').attr('stat-group');
		var statVal = self[statGroup][stat].rank();
		var statTempMod = self[statGroup][stat].tempMod;

		var modalHeader =  "<h4>"+ stat +" <span class='statVal'>"+ statVal +"</span></h4><h5>Temporary modifier: <span class='statTempMod'>"+ statTempMod +"</span></h5><br>";
		var modalBody = $("<div/>")
			.addClass('row center')
			.append(
				$("<div/>")
				.addClass('col s4')
				.append(
					$("<button/>")
					.addClass('btn red')
					.addClass('downTempMod')
					.addClass('waves-effect')
					.html("-")
					.click(function(event) {
						self[statGroup][stat].tempMod -= parseInt($("#tempModal .tempModVal").val());
						self.updateAllStats();
						$("#tempModal .statVal").text(self[statGroup][stat].rank());
						$("#tempModal .statTempMod").text(self[statGroup][stat].tempMod);
						
						if(self[statGroup][stat].tempMod != '0') {
							$("#"+self[statGroup][stat].elemId).addClass(self.theme.colorScheme.accentColorText);
						}
						else {
							$("#"+self[statGroup][stat].elemId).removeClass(self.theme.colorScheme.accentColorText);
						}
					})
				)
			)
			.append(
				$("<div/>")
				.addClass('col s4')
				.append(
					$("<input/>")
					.addClass('tempModVal')
					.attr('type', 'text')
					.attr('value', '1')
				)
			)
			.append(
				$("<div/>")
				.addClass('col s4')
				.append(
					$("<button/>")
					.addClass('btn green')
					.addClass('waves-effect')
					.addClass('upTempMod')
					.html("+")
					.click(function(event) {
						self[statGroup][stat].tempMod += parseInt($("#tempModal .tempModVal").val());
						self.updateAllStats();
						$("#tempModal .statVal").text(self[statGroup][stat].rank());
						$("#tempModal .statTempMod").text(self[statGroup][stat].tempMod);

						if(self[statGroup][stat].tempMod != '0') {
							$("#"+self[statGroup][stat].elemId).addClass(self.theme.colorScheme.accentColorText);
						}
						else {
							$("#"+self[statGroup][stat].elemId).removeClass(self.theme.colorScheme.accentColorText);
						}
					})
				)
			);

		$("#resetTempMod").click(function(event) {
			self.resetTempMod({
				stat: stat,
				statGroup: statGroup
			});
		});

		$("#tempModal").children('.modal-content').append(modalHeader).append(modalBody);

	});

	
	/*----------  event for switches that enhance stats  ----------*/
	$(document).on('change', '.enhancementLever', function(event) {
		var enhPowerComponentName = $(this).attr('power-component-name');
		var enhPowerName = $(this).parents('.power-item').attr('power-name');;
		var statGroup = $(this).attr('pseudo') || "Powers";
		var enhs;

		if(statGroup == "Powers") {
			powComp = self[statGroup][enhPowerName].PowerComponents[enhPowerComponentName];
		}
		else {
			powComp = self[statGroup][enhPowerName].effect.power.PowerComponents[enhPowerComponentName];
		}

		var pointsPerRank = (parseInt(powComp.pointsPerRank) || 1);

		// console.log(powComp);
	
		for(var i = 0; i < powComp.effect.enhancements.length; i++) {

			var enh = powComp.effect.enhancements[i];
			var startsAfter = ('startsAfter' in enh)? enh.startsAfter : 0;
			var modifier = parseInt(enh.modifier || 0);
			var value = parseInt($(this).val()) + modifier;

			if(value > startsAfter) {
				if(this.checked) {
					self[enh.statGroup][enh.stat].modifier += (value - startsAfter) / pointsPerRank;
				}
				else {
					self[enh.statGroup][enh.stat].modifier -= (value - startsAfter) / pointsPerRank;
				}
				self.updateAllStats();
			}
		}

	});

	/*---------- switch off other power components in array powers  ----------*/
	$(document).on('change', '.powerComponentLever', function(event) {
		var thisElem = $(this);

		if(this.checked) {
			var powerGroup = $(this).parents('.powerComponentsContainer');

			if(thisElem.attr('powerComponentType') == 'alternate') {
				powerGroup.find(".powerComponentLever").each(function(index, el) {
					if($(el).attr('id') != thisElem.attr('id') && $(el).prop("checked")) {
						$(el).prop("checked", false).trigger('change');
					}
				});
			}
		}

		if(thisElem.parents(".powerComponentsContainer").hasClass('dynamic')) {			
			//power total points display value
			var totalPoints = thisElem.parents(".powerComponentsContainer").siblings('.collapsible-header').find('.total-points');
			var pointsInvested = 0;

			thisElem.parents(".powerComponentsContainer").find(".powerComponentLever").each(function(index, el) {
				if(el.checked) {
					pointsInvested += parseInt($(el).val());
				}
			});
			totalPoints.fadeOut('fast', function() {
				totalPoints.text(pointsInvested);
				totalPoints.fadeIn('fast');
			});

			if(pointsInvested > totalPoints.attr('max-points')) {
				totalPoints.removeClass(self.theme.colorScheme.mainColorText);
				totalPoints.addClass('red-text');
			}
			else {
				totalPoints.addClass(self.theme.colorScheme.mainColorText);
				totalPoints.removeClass('red-text');
			}
		}
	});

	/*----------  normal powers - power switch to power components control  ----------*/
	$(document).on('change', '.normal-power .power-lever', function(event) {
		var powerId = $(this).attr("power-id");
		var thisElem = this;
		$("#"+ powerId +"-container").find(".enhancementLever").each(function(index, el) {
			if($(el).prop("checked") != thisElem.checked) {
				$(el).prop("checked", thisElem.checked).trigger('change');
			}
		});
	});

	/*----------  alternate powers - power switch to power components control  ----------*/
	$(document).on('change', '.array-power .power-lever', function(event) {
		var powerId = $(this).attr("power-id");
		var thisElem = $(this);
		$("#"+ powerId +"-container").find(".powerComponentLever").each(function(index, el) {
			if($(el).prop("checked")) {
				$(el).prop("checked", false).trigger('change');
			}
		});
		if(this.checked) {
			$("#"+ powerId +"-container").find(".powerComponentLever").first().prop("checked", true).trigger('change');
		}
	});

	/*----------  dynamic powers - power switch to power components control  ----------*/
	$(document).on('change', '.dynamic-power .power-lever', function(event) {
		var powerId = $(this).attr("power-id");
		var thisElem = $(this);
		$("#"+ powerId +"-container").find(".powerComponentLever").each(function(index, el) {
			if($(el).prop("checked")) {
				$(el).prop("checked", false).trigger('change');
			}
		});

		if(this.checked) {
			$("#"+ powerId +"-container").find(".powerComponentLever").first().prop("checked", true).trigger('change');
			var item = $("#"+ powerId +"-container").find(".collection-item").first();

			if(item.hasClass('dynamic')) {
				item.find('.btn.max-dynamic-component').trigger('click');
			}

		}

		//power total points display value
		var totalPoints = thisElem.parents(".powerComponentsContainer").siblings('.collapsible-header').find('.total-points');
		var pointsInvested = 0;

		thisElem.parents(".powerComponentsContainer").find(".powerComponentLever").each(function(index, el) {
			if(el.checked) {
				pointsInvested += parseInt($(el).val());
			}
		});
		totalPoints.fadeOut('fast', function() {
			totalPoints.text(pointsInvested);
			totalPoints.fadeIn('fast');
		});

		if(pointsInvested > totalPoints.attr('max-points')) {
			totalPoints.removeClass(self.theme.colorScheme.mainColorText);
			totalPoints.addClass('red-text');
		}
		else {
			totalPoints.addClass(self.theme.colorScheme.mainColorText);
			totalPoints.removeClass('red-text');
		}
	});


	/*----------  dynamic powers - range slider event controller  ----------*/
	$(document).on('input', '.dynamic-power .powerComponentRange', function(event) {
		var powerComponentName = $(this).attr("powerComponent-name");
		var thisElem = $(this);
		var pcLever = $("#"+powerComponentName);
		var totalPoints = thisElem.parents(".powerComponentsContainer").siblings('.collapsible-header').find('.total-points');
		var pointsInvested = 0;

		//power component display points invested
		$("#"+ powerComponentName +"-val").text(thisElem.val());
		
		if($(pcLever).prop('checked')) {
			$(pcLever).prop('checked', false).trigger('change');
		}
		$(pcLever).val(thisElem.val());
		$(pcLever).prop("checked", (((parseInt(thisElem.val()) < 1))? false : true) ).trigger('change');
	
		//power total points display value
		thisElem.parents(".powerComponentsContainer").find(".powerComponentLever").each(function(index, el) {
			if(el.checked) {
				pointsInvested += parseInt($(el).val());
			}
		});
		totalPoints.fadeOut('fast', function() {
			totalPoints.text(pointsInvested);
			totalPoints.fadeIn('fast');
		});

		if(pointsInvested > totalPoints.attr('max-points')) {
			totalPoints.removeClass(self.theme.colorScheme.mainColorText);
			totalPoints.addClass('red-text');
		}
		else {
			totalPoints.addClass(self.theme.colorScheme.mainColorText);
			totalPoints.removeClass('red-text');
		}
	});

	/*----------  dynamic powers - max button event controller  ----------*/
	$(document).on('click', '.dynamic-power .max-dynamic-component', function(event) {
		var powerComponentName = $(this).attr("powerComponent-name");
		var range = $("#"+powerComponentName+"-slider");

		range.val(range.attr('max')).trigger('input');

	});


	/*============  dice modal content control  ============*/
	$(document).on('click', '.modal-trigger.interaction-trigger', function(event) {
		$("#diceModal").children('.modal-content').html('');

		var stat = $(this).attr('stat');
		var statGroup = $(this).attr('stat-group');
		var statVal = self[statGroup][stat].rank();

		var modalHeader =  "<h4>"+ stat +" check</h4>";
		var diceRoll = Math.floor(Math.random() * 20) + 1;
		var roll =  parseInt(diceRoll) + parseInt(statVal);
		var rollClass = "";
		var criticalThreat = 0;

		if ('improvedCritical' in self[statGroup][stat]) {
			criticalThreat = parseInt(self[statGroup][stat].improvedCritical);
		}

		if(diceRoll >= (20 - criticalThreat)) {
			rollClass = self.theme.colorScheme.accentColorText;
		}

		var modalBody = "<h3>"+ roll +" = [<span class='"+ rollClass +"'>"+ diceRoll +"</span>] + "+ statVal +"</h3>";

		$("#diceModal").children('.modal-content').append(modalHeader).append(modalBody);

		$("#historyContainer").prepend(modalBody).prepend(modalHeader).prepend('<hr/>');
	});


	/**
	 * =============================
	 * Power Checks event Controller
	 * =============================
	 */
	$(document).on('click', '.modal-trigger.power-interaction', function(event) {
		$("#diceModal").children('.modal-content').html('');

		var statGroup = $(this).attr('pseudo') || "Powers";
		var powerName = $(this).attr('power-name');
		var powerComponentName = $(this).attr('power-component-name');
		var powerComponent = self.getPowerComponentByName({
			statGroup: statGroup,
			powerName: powerName,
			powerComponentName: powerComponentName
		});

		var effects = powerComponent.effect;
		var powerEffects = ['affliction', 'damage', 'secondary-damage', 'nullify', 'weaken'];

		var modifier = parseInt(effects.interaction.modifier || 0);
		var modifierText = "";
		var criticalThreat = parseInt(effects.interaction.improvedCritical || 0);
		var diceRoll = Math.floor(Math.random() * 20) + 1;
		var roll, rollClass = "";
		var modalHeader =  $("<h4/>").append(powerComponentName);
		var modalBody = $("<div/>");
		var effectsListHtml = $("<div/>").addClass('effects-list');

		
		// if interaction has charges
		if ('charges' in effects.interaction) {
			var chargesModifier = parseInt(effects.interaction.charges.modifier || 0);
			var charges = parseInt(effects.interaction.charges.count) + chargesModifier;
			
			if(charges > 0) {
				chargesModifier -= 1;
				self.updatePowerComponentChargesModifier({
					statGroup: statGroup,
					powerName: powerName,
					powerComponentName: powerComponentName,
					modifier: chargesModifier,
					triggerElem: this
				});	
			}
			else {
				modalBody.append($("<h5/>").append('Out of charges!'));

				$("#diceModal").children('.modal-content').append(modalHeader).append(modalBody);
				$("#historyContainer").prepend(modalBody).prepend(modalHeader).prepend('<hr/>');
				return true;
			}
		}

		// if interaction is tied to a stat
		if ('stat' in effects.interaction) {
			var stat = effects.interaction.stat;
			var statGroup = effects.interaction.statGroup;

			modifier += self[statGroup][stat].rank();

			if ('improvedCritical' in self[statGroup][stat]) {
				criticalThreat += parseInt(self[statGroup][stat].improvedCritical);
			}
		}

		// if interaction is tied to a power
		if ('power' in effects.interaction) {
			var pow = effects.interaction.power;
			var powComp = effects.interaction.powerComponent;

			modifier += self.Powers[pow].PowerComponents[powComp].rank;
		}

		roll =  parseInt(diceRoll) + parseInt(modifier);

		if(diceRoll >= (20 - criticalThreat)) {
			rollClass = self.theme.colorScheme.accentColorText;
		}
	
		if (modifier > 0) {
			modifierText = " + " + modifier;
		}

		switch(effects.interaction.type) {
			case "noroll":
				break;

			case "luck": 
				if(diceRoll <= 10) {
					roll = parseInt(diceRoll + 10);
					// if(reroll >= (20 - criticalThreat)) {
					// 	rollClass = self.theme.colorScheme.accentColorText;
					// }
					modalBody.append($("<h3/>").append(parseInt(roll + modifier) +" = [<span class='"+ rollClass +"'>"+ diceRoll + "->" + roll +"</span>]" + modifierText));
				}
				else {
					modalBody.append($("<h3/>").append(roll +" = [<span class='"+ rollClass +"'>"+ diceRoll +"</span>]" + modifierText));
				}
				break;

			case "normal":
			case "attack":
			default:
				modalBody.append($("<h3/>").append(roll +" = [<span class='"+ rollClass +"'>"+ diceRoll +"</span>]"+ modifierText));
				break;
		}
		
		var hist = modalBody.clone();
		modalBody.append(effects.description);

		$.each(effects, function(index, effect) {
			
			if(powerEffects.indexOf(index) > -1) {
				var effectRanks = parseInt(effect.modifier || 0);
				var extraInfo = "";
				
				if('stat' in effect) {
					effectRanks += self[effect.statGroup][effect.stat].rank();
				}

				switch(index) {
	    			case "affliction":
	    			case "weaken":
	    				extraInfo = " | DC "+ parseInt(effectRanks + 10);
	    				break;

	    			case "damage":
	    			case "secondary-damage":
	    				extraInfo = " | DC "+ parseInt(effectRanks + 15);
	    				break;

	    			case "nullify":
	    				var nullifyRoll = Math.floor(Math.random() * 20) + 1;
	    				var nullifyCheck = parseInt(nullifyRoll) + parseInt(effectRanks);
	    				extraInfo = " | check: "+ nullifyCheck +" = ["+ nullifyRoll +"] + "+ effectRanks;
	    				break;
	    		}
					
				effectsListHtml.append(
					$('<li/>')
					.html((index.charAt(0).toUpperCase() + index.slice(1)) +': '+ effectRanks + " effect ranks" + extraInfo)
				);
			}
		
		});

		modalBody.append(effectsListHtml);

		$("#diceModal").children('.modal-content').append(modalHeader[0].outerHTML).append(modalBody[0].outerHTML);
		$("#historyContainer").prepend(hist).prepend(modalHeader).prepend('<hr/>');
	});

	// rest character
	$(document).on('click', '#rest', function(event) {
		$("#diceModal").children('.modal-content').html('');

		self.rest();

		var modalHeader = "";
        var modalBody = "<h4 class='"+ self.theme.colorScheme.accentColorText +"'>Character has rested</h4>";
        
        $("#diceModal").children('.modal-content').append(modalHeader).append(modalBody);
        $("#historyContainer").prepend(modalBody).prepend(modalHeader).prepend('<hr/>');
	});

	$(document).on('click', '.reset-charges', function(event) {
		$("#diceModal").children('.modal-content').html('');

		self.updatePowerComponentChargesModifier({
			statGroup: $(this).attr('pseudo') || "Powers",
			powerName: $(this).attr('power-name'),
			powerComponentName: $(this).attr('power-component-name'),
			modifier: 0,
			triggerElem: this
		});	

		var modalHeader = "<h4 class='"+ self.theme.colorScheme.accentColorText +"'>"+ $(this).attr('power-component-name') +"</h4>";
        var modalBody = "<h5 class='"+ self.theme.colorScheme.accentColorText +"'>Character has recovered all charges</h5>";
        
        $("#diceModal").children('.modal-content').append(modalHeader).append(modalBody);
        $("#historyContainer").prepend(modalBody).prepend(modalHeader).prepend('<hr/>');
	});

	// simple d20 roll
	$(document).on('click', '#d20roll', function(event) {
        $("#diceModal").children('.modal-content').html('');

        var modalHeader =  "<h4>d20 Roll</h4>";
        var diceRoll = Math.floor(Math.random() * 20) + 1;
        var rollClass = "";
        var criticalThreat = 0;

        if(diceRoll >= (20 - criticalThreat)) {
            rollClass = self.theme.colorScheme.accentColorText;
        }

        var modalBody = "<h3>[<span class='"+ rollClass +"'>"+ diceRoll +"</span>]</h3>";
        
        $("#diceModal").children('.modal-content').append(modalHeader).append(modalBody);
        $("#historyContainer").prepend(modalBody).prepend(modalHeader).prepend('<hr/>');
    });

	//hidden button to trigger all powers
	$(document).on('click', '#powers>h4', function(event) {
		$("#powers .power-lever").each(function(index, el) {
			$(el).trigger('click');
		});
	});

	// init all powers
	$("#powers .power-lever").each(function(index, el) {
		if($(el).attr('power-id').includes("Power-Stunts")) {
			return true;
		}

		$(el).trigger('click');
	});

}

/**
 * get the character as JSON string
 * @return {JSON} [description]
 */
Character.prototype.getCharcterAsJSON = function() {
	var self = this;
	var characterJSON = {}
	, abilitiesJSON = {}
	, defensesJSON = {}
	, skillsJSON = {};

	for(var index in self.Abilities) {
		var ability = self.Abilities[index];
		abilitiesJSON[ability.name] = {
			"baseValue": ability.baseValue,
			"modifier": ability.modifier
		};
	}

	for(var index in self.Defenses) {
		var defense = self.Defenses[index];
		defensesJSON[defense.name] = {
			"modifier": defense.modifier
		};
	}

	for(var index in self.Skills) {
		var skill = self.Skills[index];
		skillsJSON[skill.name] = {
			"ability": skill.ability.name,
			"trained": skill.trained,
			"modifier": skill.modifier
		};
	}

	characterJSON = {
		"Name": self.Name,
		"PowerLevel": self.PowerLevel,
		"Abilities": abilitiesJSON,
		"Defenses": defensesJSON,
		"Skills": skillsJSON,
	};

	return characterJSON;
}

/**
 * updates the character JSON stored in the localStorage
 * @return {string} the stored string
 */
Character.prototype.updateStoredCharacter = function() {
	var self = this;
	localStorage.setItem(self.Name + "Test", JSON.stringify(self.getCharcterAsJSON()));

	return localStorage.getItem(self.Name + "Test");
}

/* ================== getters and setters ================== */

/**
 * returns ability value
 * @param  {string} ability [description]
 * @return {number}         [description]
 */
Character.prototype.getAbility = function(ability) {
	var self = this;

	return (self.Abilities[ability].rank());
}

/**
 * sets the bonus value for an ability
 * @param {[type]} ability [description]
 * @param {[type]} value   [description]
 */
Character.prototype.setAbility = function(ability, value) {
	var self = this;

	self.Abilities[ability].modifier = value;
	
	// self.updateStoredCharacter();
	self.updateAbilities();
}

/**
 * sets the base value of an ability
 * @param {[type]} ability [description]
 * @param {[type]} value   [description]
 */
Character.prototype.setBaseAbility = function(ability, value) {
	var self = this;

	self.Abilities[ability].baseValue = value;
	
	self.updateStoredCharacter();
	self.updateAbilities();
}

/**
 * updates the abilities html values
 * @return {[type]}      [description]
 */
Character.prototype.updateAbilities = function() {
	var self = this;

	$.each(self.Abilities, function(index, Ability) {
		if($('#'+Ability.elemId).text() != Ability.rank()) {
			$('#'+Ability.elemId).fadeOut("", function() {
				$(this).text(Ability.rank());
				$('#'+Ability.elemId).fadeIn();
			});
		}
	});
	self.updateDefences();
	self.updateSkills();
}

/**
 * updates the defenses html values
 * @return {[type]} [description]
 */
Character.prototype.updateDefences = function() {
	var self = this;

	$.each(self.Defenses, function(index, Defense) {
		if($('#'+Defense.elemId).text() != Defense.rank()) {
			$('#'+Defense.elemId).fadeOut("", function() {
				$(this).text(Defense.rank());
				$('#'+Defense.elemId).fadeIn();
			});
		}
	});
}

/**
 * updates the skills html values
 * @return {[type]} [description]
 */
Character.prototype.updateSkills = function() {
	var self = this;

	$.each(self.Skills, function(index, Skill) {
		if($('#'+Skill.elemId).text() != Skill.rank()) {
			$('#'+Skill.elemId).fadeOut("", function() {
				$(this).text(Skill.rank());
				$('#'+Skill.elemId).fadeIn();
			});
		}
	});
}

/**
 * updates all stat html values
 */
Character.prototype.updateAllStats = function() {
	var self = this;

	self.updateAbilities();
}

/**
 * get the power component by statGroup and powerName
 * @param {object} args	[description]
 * @param {object} value	[description]
 */
Character.prototype.getPowerComponentByName = function(args) {
	var self = this;

	var powerComponent = {};

	if(args.statGroup == "Powers") {
		powerComponent = self[args.statGroup][args.powerName].PowerComponents[args.powerComponentName];
	}
	if(args.statGroup == "Advantages") {
		powerComponent = self[args.statGroup][args.powerName].effect.power.PowerComponents[args.powerComponentName];
	}

	return powerComponent;

}

/**
 * updates charges for power component
 * @return {[type]} [description]
 */
Character.prototype.updatePowerComponentChargesModifier = function(args) {
	var self = this;

	if(args.statGroup == "Powers") {
		self[args.statGroup][args.powerName].PowerComponents[args.powerComponentName].effect.interaction.charges.modifier = args.modifier;
	}
	if(args.statGroup == "Advantages") {
		self[args.statGroup][args.powerName].effect.power.PowerComponents[args.powerComponentName].effect.interaction.charges.modifier = args.modifier;
	}

	self.updatePowerComponentChargesHtml(args);
}

/**
 * update the charges html text
 * @param  {[type]} args [description]
 */
Character.prototype.updatePowerComponentChargesHtml = function(args) {
	var self = this;

	var powerComponent = self.getPowerComponentByName({
		statGroup: args.statGroup,
		powerName: args.powerName,
		powerComponentName: args.powerComponentName
	})

	// $(args.triggerElem).each(function(index, el) {
	$(".power-interaction[power-name='"+ args.powerName +"'][power-component-name='"+ args.powerComponentName +"']").each(function(index, el) {
		$(el).find(".powCount").text(parseInt(powerComponent.effect.interaction.charges.count) + parseInt(powerComponent.effect.interaction.charges.modifier));
		// console.log($(el).find(".powCount"), powerComponent.effect.interaction.charges.count);
	});
}

/**
 * resets all charges
 */
Character.prototype.resetCharges = function() {
	var self = this;

	$(".power-interaction[has-charges='true']").each(function(index, el) {

		self.updatePowerComponentChargesModifier({
			statGroup: $(el).attr('pseudo') || "Powers",
			powerName: $(el).attr('power-name'),
			powerComponentName: $(el).attr('power-component-name'),
			modifier: 0,
			triggerElem: this
		});	
	});
}

/**
 * resets one temporary modifier
 */
Character.prototype.resetTempMod = function(args) {
	var self = this;

	self[args.statGroup][args.stat].tempMod = 0;
	self.updateAllStats();

	$("#"+self[args.statGroup][args.stat].elemId).removeClass(self.theme.colorScheme.accentColorText);
}

/**
 * resets all temporary modifiers
 */
Character.prototype.resetTempMods = function() {
	var self = this;

	$.each(self.Abilities, function(index, Ability) {
		Ability.tempMod = 0;
		$("#"+Ability.elemId).removeClass(self.theme.colorScheme.accentColorText);
	});

	$.each(self.Defenses, function(index, Defense) {
		Defense.tempMod = 0;
		$("#"+Defense.elemId).removeClass(self.theme.colorScheme.accentColorText);
	});

	$.each(self.Skills, function(index, Skill) {
		Skill.tempMod = 0;
		$("#"+Skill.elemId).removeClass(self.theme.colorScheme.accentColorText);
	});

	self.updateAllStats();
}

/**
 * updates all stat html values
 */
Character.prototype.rest = function() {
	var self = this;

	self.resetCharges();
	self.resetTempMods();
}


/* ================== Character HTML sections generation ================== */

/**
 * generates and appends the abilities html to the page
 */
Character.prototype.getAbilitiesSectionHTML = function() {
	var self = this;
	var list = [];
	var html;
	var row = $("<div>", {
		"class": "row"
	});
	var abilitiesList = $("<div>", {
		"class": "abilitiesList"
	});

	$("#abilitiesContainer").append(
		$('<h4/>')
			.html("Abilities")
	);
	$("#abilitiesContainer").append("<hr>");

	$.each(self.Abilities, function(index, Ability) {
		list.push(Ability.getHTML());
	});

	for (var i = 0; i < list.length; i++) {
		
		var col = $("<div>", {
			"class": "col s6"
		});
		col.append(list[i])

		if(i % 2 == 0) {
			row = $("<div>", {
				"class": "row"
			});			
			row.append(col);
		}
		else {
			row.append(col);
			abilitiesList.append(row);
		}
	}

	$("#abilitiesContainer").append(abilitiesList);
}

/**
 * generates and appends the defenses html to the page
 */
Character.prototype.getDefensesSectionHTML = function() {
	var self = this;
	var list = [];
	var html;
	var row = $("<div>", {
		"class": "row"
	});
	var defensesList = $("<div>", {
		"class": "defensesList"
	});

	$("#defensesContainer").append(
		$('<h4/>')
			.html("Defenses")
	);
	$("#defensesContainer").append("<hr>");

	$.each(self.Defenses, function(index, Defenses) {
		list.push(Defenses.getHTML());
	});

	for (var i = 0; i < list.length; i++) {
		
		var col = $("<div>", {
			"class": "col s6"
		});
		col.append(list[i])

		if(i % 2 == 0) {
			row = $("<div>", {
				"class": "row"
			});			
			row.append(col);
		}
		else {
			row.append(col);
			defensesList.append(row);
		}
	}

	$("#defensesContainer").append(defensesList);
}

/**
 * generates and appends the skills html to the page
 */
Character.prototype.getSkillsSectionHTML = function() {
	var self = this;
	var list = [];
	var html;
	var row = $("<div>", {
		"class": "row"
	});
	var skillsList = $("<div>", {
		"class": "skillsList"
	});

	$.each(self.Skills, function(index, Skill) {
		list.push(Skill.getHTML());
	});

	for (var i = 0; i < list.length; i++) {
		
		var col = $("<div>", {
			"class": "col s12"
		});
		col.append(list[i]);
	
		row.append(col);
		skillsList.append(row);
		
	}

	$("#skillsContainer").append(skillsList);
}

/**
 * generates and appends the powers html to the page
 */
Character.prototype.getPowersSectionCollapsibleHTML = function() {
	var self = this;

	var powItems = [];
	var collapsibleList = $("<ul>", {
		"class": "collapsible popout",
		"data-collapsible": "expandable"

	});

	$.each(self.Powers, function(index, Power) {
		collapsibleList.append(Power.getHTML());
	});

	$(".powersContainer").append(collapsibleList);
}

/**
 * generates and appends the advantages html to the page
 */
Character.prototype.getAdvantagesSectionHTML = function() {
	var self = this;
	
	var advantagesList = $("<ul/>")
		.addClass('collection')
		.attr('id', 'advantagesList');

	$.each(self.Advantages, function(index, Advantage) {
		advantagesList.append(Advantage.getHTML());
	});

	$(".advantagesContainer").append(advantagesList);

}

/**
 * generates and appends the bio and complications html to the page
 */
Character.prototype.getBioSectionHTML = function() {
	var self = this;

	$(".character-bio").html(self.Bio);
	$(".complications").html(self.Complications);
}


/**
 * sets colors and images in the page
 */
Character.prototype.setTheme = function() {
	var self = this;

	var mainColor = self.theme.colorScheme.mainColor;
	var mainColorText = self.theme.colorScheme.mainColorText;
	var accentColor = self.theme.colorScheme.accentColor;
	var accentColorText = self.theme.colorScheme.accentColorText;

	$("#mobile-header").attr('content', mainColor);

	$("#logo-container").html(self.Name);

	$("#profileBanner").attr('src', self.theme.images.banner);
	$("#profilePicture").attr('src', self.theme.images.profile);
	$("#profileName").text(self.Identity +" / "+ self.Name);
	$("#parallaxImg").attr('src', self.theme.images.parallax);

	// $(".user-view .background").addClass(accentColor);
	// $("#slide-out").addClass(accentColor);
	$("#slide-out li.active>a").addClass(mainColorText);

	$("#page-top").addClass(mainColor);
	$("#page-top .tabs").addClass(mainColor);

	$(".reset-charges").addClass(accentColor);

	$("#powers>h4").addClass(mainColorText);
	$("#advantages>h4").addClass(mainColorText);

	$(".fixed-action-btn a").addClass(accentColor);
	$("#diceModal").addClass(mainColorText);
	$("#historyModal .modal-content>h4").addClass(mainColorText);

	$(".page-footer").addClass(mainColor);
	$(".page-footer ul>li>a").addClass(accentColorText);

	$(".footer-name").html(self.Name);
	$(".footer-identity").text(self.Identity);
}