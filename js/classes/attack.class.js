
function Attack(args) {
	var self = this;
	this.name = args.name;
	this.elemId = convertToId(self.name);
	this.ability = args.ability;
	this.modifier = parseInt(args.modifier || 0);
	this.rank = function() {
		return parseInt(self.ability.rank()) + parseInt(self.modifier);
	};
}

Attack.prototype.getHTML = function() {
	var self = this;

	var html = '	\
	<div class="row">	\
	    <div class="col s9 attackLabel">	\
	        <a class="modal-trigger interaction-trigger" href="#diceModal" stat-group="" stat=""><span class="">'+self.name+'</span></a>	\
	    </div>	\
	    <div class="col s3 attackVal">	\
	        <span id="'+self.elemId+'" class="">'+self.rank()+'</span>	\
	    </div>	\
	</div>';

	return html;
}

Attack.prototype.updateAttack = function(args) {
	var self = this;

}