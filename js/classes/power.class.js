
function Power(args) {
	var self = this;
	this.name = args.name;
	this.elemId = convertToId(self.name);
	this.icon = args.icon;
	this.colorScheme = args.colorScheme;
	this.type = args.type;
	this.pseudo = args.pseudo || "";
	this.powerPoints = parseInt(args.powerPoints);

	this.PowerComponents = {};

	for(var index in args.PowerComponents) {
		var pComp = args.PowerComponents[index];
		var rank = 1;

		if(typeof(pComp.rank) !== 'undefined') {
			rank = parseInt(pComp.rank);
		}

		self.PowerComponents[index] = new PowerComponent({
			name: index,
			parentPowerName: self.name,
			colorScheme: self.colorScheme,
			type: pComp.type || "normal", // possible options {normal, array, dynamic}
			pseudo: self.pseudo,
			rank: rank, // effect rank
			pointsPerRank: parseInt(pComp.pointsPerRank || 1),
			minVal: parseInt(pComp.minVal || 0),
			maxVal: parseInt(pComp.maxVal || self.powerPoints),
			effect: pComp.effect
		});
	}
}

Power.prototype.getHTML = function() {
	var self = this;
	var icon = "";
	var powerPoints = "";
	var showSwitch = false;
	var powerComponentsHTML = $("<div>", {
		"class": "container"
	});

	if(self.icon != "") {
		icon = '<i class="material-icons">'+ self.icon +'</i>';
	}

	if(self.type == 'dynamic') {
		powerPoints = "&nbsp<[<span class='total-points "+ self.colorScheme.mainColorText +"' max-points='"+ self.powerPoints +"'>"+ (self.powerPoints-self.powerPoints) +"</span>/"+ self.powerPoints + "]>";
	}

	for(var index in self.PowerComponents) {
		var pComp = self.PowerComponents[index];
		powerComponentsHTML.append(pComp.getHTML());
		
		if(typeof(pComp.effect.enhancements) !== "undefined") {
			showSwitch = true;
		}
	}

	var html = '	\
	<li class="'+self.type+'-power power-item" power-name="'+ self.name +'">	\
		<div class="collapsible-header '+ ((self.pseudo == "Advantages")? self.colorScheme.mainColorText : '') +'">'+ icon + '<span class="">' + self.name + powerPoints +'</span></div>	\
		<div class="powerSwitch '+ (showSwitch? '' : 'hide') +'">	\
			<div class="switch">	\
				<label>	\
					<input type="checkbox" class="power-lever" power-id="'+ self.elemId +'">	\
					<span class="lever '+ self.colorScheme.mainColor +' lighten-3"></span>	\
				</label>	\
			</div>	\
		</div>	\
		<div class="collapsible-body powerComponentsContainer '+self.type+'">	\
			<ul class="collection" id="'+ self.elemId +'-container">'+ powerComponentsHTML.html() +'</ul>	\
		</div>	\
	</li>';

	return html;
}