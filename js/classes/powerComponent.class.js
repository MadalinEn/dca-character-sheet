
function PowerComponent(args) {
	var self = this;
	this.name = args.name;
	this.parentPowerName = args.parentPowerName;
	this.elemId = convertToId(self.name);
	this.colorScheme = args.colorScheme;
	this.type = args.type; // possible options {normal, alternate, dynamic, none?}
	this.pseudo = args.pseudo;
	this.rank = parseInt(args.rank); // effect rank (for normal effects)
	this.pointsPerRank = parseInt(args.pointsPerRank || 1); // for dynamic powerComps
	this.minVal = parseInt(args.minVal); // for dynamic powerComps
	this.maxVal = parseInt(args.maxVal); // for dynamic powerComps
	this.effect = args.effect;

}

PowerComponent.prototype.getHTML = function() {
	var self = this;

	var itemContainer = $("<li/>")
		.addClass('collection-item')
		.addClass(self.type);

	var powComp = $("<div/>")
		.addClass('power-component');

	var powHeader = self.name;

	if('interaction' in self.effect) {
		powHeader = $("<a/>")
			.addClass('modal-trigger')
			.addClass('power-interaction')
			.addClass(self.colorScheme.accentColorText)
			.attr('href', '#diceModal')
			.attr("pseudo", self.pseudo)
			.attr("power-name", self.parentPowerName)
			.attr("power-component-name", self.name);

		powHeader.append(self.name);

		if('charges' in self.effect.interaction) {
			powHeader.attr('has-charges', true);
			powHeader.append("&nbsp&nbsp(<span class='powCount'>"+ self.effect.interaction.charges.count +"</span>/"+ self.effect.interaction.charges.count +")" );
			
			resetButton = $("<button/>")
				.addClass('btn waves-effect')
				.addClass('reset-charges')
				.addClass('modal-trigger')
				.attr('href', '#diceModal')
				.attr("pseudo", self.pseudo)
				.attr("power-name", self.parentPowerName)
				.attr("power-component-name", self.name)
				.html('<i class="material-icons">replay</i>');

			$('#character-counters').append(
				$("<li/>").append(powHeader[0].outerHTML).append(resetButton)
			);
		}
	}

	switch(self.type) {
	    case "normal":
	    	powComp.append(powHeader);

	    	if(typeof(self.effect.enhancements) !== "undefined") {
	    		powComp.append(
	    			$("<div/>")
    				.addClass("secondary-content powerComponentSwitch")
    				.append(
    					$("<div/>")
    					.addClass("switch")
    					.append(
    						$("<label/>")
	    					.append(
			    				$("<input/>")
		    					.attr("type", "checkbox")
		    					.attr("id", self.elemId)
		    					.attr("value", self.rank)
		    					.attr("pseudo", self.pseudo)
		    					.attr("power-component-name", self.name)
		    					.attr("pointsPerRank", self.pointsPerRank)
		    					.addClass('powerComponentLever')
		    					.addClass("enhancementLever")
		    				)
			    			.append(
	    						$("<span/>")
	    						.addClass("lever")
	    						.addClass(self.colorScheme.mainColor +" lighten-3")
	    					)
			    		)
    				)
	    		);
	    	}

	    	break;

	    case "alternate": 
	        
	    	powComp.append(powHeader);

	    	var lever = $("<input/>")
				.attr("type", "checkbox")
				.attr("id", self.elemId)
				.attr("value", Math.max(self.rank, self.maxVal))
				.attr("pointsPerRank", self.pointsPerRank)
				.attr('powerComponentType', self.type)
				.addClass('powerComponentLever')

			if(typeof(self.effect.enhancements) !== "undefined") {
				lever.addClass("enhancementLever")
					.attr("pseudo", self.pseudo)
					.attr("power-component-name", self.name)
			}

    		powComp.append(
    			$("<div/>")
				.addClass("secondary-content powerComponentSwitch")
				.append(
					$("<div/>")
					.addClass("switch")
					.append(
						$("<label/>")
    					.append(lever)
		    			.append(
    						$("<span/>")
    						.addClass("lever")
    						.addClass(self.colorScheme.mainColor +" lighten-3")
    					)
		    		)
				)
    		);

	        break;

	    case "dynamic":

	    	var collapsible = $("<ul/>")
	    		.addClass('collapsible')
	    		.attr('data-collapsible', 'expandable');

	    	var listItem =  $("<li/>");

	    	var collapsibleHeader = $("<div/>")
	    		.addClass('collapsible-header')
	    		.addClass(self.colorScheme.mainColorText)
	    		.append($("<div/>")
	    			.addClass('width-85')
	    			.addClass('truncate')
		    		.html(powHeader)
		    		.append('&nbsp<[')
	    			.append(
	    				$("<span/>")
	    				.attr('id', self.elemId +'-val')
	    				.text(self.minVal)
	    			)
	    			.append(']>')
	    		);
	    		

	    	var lever = $("<input/>")
				.attr("type", "checkbox")
				.attr("id", self.elemId)
				.attr("value", self.minVal)
				.attr("pointsPerRank", self.pointsPerRank)
				.attr('powerComponentType', self.type)
				.addClass('powerComponentLever');

			if(typeof(self.effect.enhancements) !== "undefined") {
				lever.addClass("enhancementLever")
					.attr("power-component-name", self.name)
					.attr("starts-after", (self.effect.enhancements.startsAfter || 0));	
			}

	    	var powerSwitch = $("<div/>")
				.addClass("powerSwitch")
				.append(
					$("<div/>")
					.addClass("switch")
					.append(
						$("<label/>")
    					.append(lever)
		    			.append(
    						$("<span/>")
    						.addClass("lever")
    						.addClass(self.colorScheme.mainColor +" lighten-3")
    					)
		    		)
				);

			var collapsibleBody = $("<div/>")
	    		.addClass('collapsible-body')
	    		.append(
	    			$("<div/>")
					.addClass("row")
					.append(
						$("<div/>")
						.addClass("col s8")
						.append(
			    			$("<div/>")
							.addClass("row")
							.addClass('dynamic-component-slider')
							.append(
								$("<div/>")
								.addClass("col s12")
								.append(
									$("<div/>")
									.addClass("range-field")
									.append(
										$("<input/>")
										.attr("type", "range")
										.attr('min', self.minVal)
										.attr('max', self.maxVal)
										.attr('step', self.pointsPerRank)
										.attr('value', self.minVal)
										.attr('id', self.elemId +'-slider')
										.attr('powerComponent-name', self.elemId)
										.addClass(self.colorScheme.mainColor)
										.addClass('range-slider')
										.addClass('powerComponentRange')
									)
								)
							)
						)
					)
					.append(
						$("<div/>")
						.addClass("col s4 right-text")
						.append(
							$("<button/>")
							.attr('powerComponent-name', self.elemId)
							.addClass("btn waves-effect")
							.addClass('max-dynamic-component')
							.addClass(self.colorScheme.mainColor)
							.html('Max')
						)
					)
				)
	    		
	    	listItem.append(collapsibleHeader).append(powerSwitch).append(collapsibleBody);

	    	collapsible.append(listItem);

			powComp.append(collapsible);


	        break;
	}

	itemContainer.append(powComp);

	return itemContainer;
}
