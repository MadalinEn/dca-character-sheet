$( document ).ready(function() {

    $('#diceModal').modal({
        // dismissible: true, // Modal can be dismissed by clicking outside of the modal
        // opacity: .5, // Opacity of modal background
        // inDuration: 300, // Transition in duration
        // outDuration: 200, // Transition out duration
        // startingTop: '4%', // Starting top style attribute
        // endingTop: '10%', // Ending top style attribute
        ready: function(modal, trigger) {}, // Callback for Modal open. Modal and trigger parameters available.
        complete: function(modal) {
            $(modal).children('.modal-content').html("");
        } // Callback for Modal close
    });

    $('#tempModal').modal();
    
    $('#historyModal').modal();

    $(document).on('click', '#clearHistory', function(event) {
        $('#historyContainer').html('');
    });

});



// localStorage.setItem('testObject', JSON.stringify(MaverickJSON));

// var retrievedObject = localStorage.getItem('testObject');

// console.log("retrievedObject", retrievedObject);


var nCharacter = new Character(CharacterJSON);

nCharacter.init();
