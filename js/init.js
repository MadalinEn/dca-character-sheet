(function($){
  $(function(){

  	jQuery.fn.outerHTML = function() {
		return jQuery('<div />').append(this.eq(0).clone()).html();
  	};

    $('.button-collapse').sideNav();
    $('ul.tabs').tabs({
    	swipeable: false
    });
    $('.parallax').parallax();

    $(".nav-link").on('click', function(event) {
		$(".nav-link").each(function(index, el) {
			$(this).removeClass('active');
		});
		$(this).addClass('active');
	});

  }); // end of document ready
})(jQuery); // end of jQuery name space

function convertToId(args) {

	var id = args.replace(/\s/g, "-");
	id = id.replace(/\[(.*?)\]/g,"$1");

	return id;
}

function convertToName(args) {

	var name = args.replace(/-/g, " ");

	return name;
}